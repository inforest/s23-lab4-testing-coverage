# Lab4 -- Testing coverage 

## Homework

As a homework you will need to develop full branch coverage for function `UserList`(in `ForumDAO.java`), full MC/DC coverage for function `Change`(in `UserDAO.java`), full Basis path coverage for function `setPost`(in `PostDAO.java`) and full statement coverage for function `treeSort` (in `ThreadDAO.java`). After that you should add all your tests to the pipeline as a different pipes. All those pipes should run concurrently(as shown on the lab). **P.S. Develop tests for different classes in different files.**
**Lab is counted as done, if pipelines are passing. and tests are developed**

