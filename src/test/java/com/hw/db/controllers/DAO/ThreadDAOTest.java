package com.hw.db.controllers.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;


public class ThreadDAOTest {

    private JdbcTemplate jdbcMock;
    private ThreadDAO threadDAO;

    @BeforeEach
    void init() {
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        threadDAO = new ThreadDAO(jdbcMock);
    }

    private static Stream<Arguments> testArguments() {
        String sqlQuery1 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;";
        String sqlQuery2 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;";
        return Stream.of(
                Arguments.of(1, 1, 1, false, sqlQuery1),
                Arguments.of(1, 1, 1, true, sqlQuery2)
        );
    }

    @ParameterizedTest
    @MethodSource("testArguments")
    void TestUserList(Integer id, Integer limit, Integer since, Boolean desc, String expected) {
        ThreadDAO.treeSort(id, limit, since, desc);
        Mockito.verify(jdbcMock).query(Mockito.eq(expected), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
    }
}
