package com.hw.db.controllers.DAO;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;
import java.util.stream.Stream;


public class UserDAOTest {
    
    private JdbcTemplate jdbcTemplateMock;
    private UserDAO userDAO;
    
    private static String nickname = "ilsur";

    @BeforeEach
    void init() {
        jdbcTemplateMock = Mockito.mock(JdbcTemplate.class);
        userDAO = new UserDAO(jdbcTemplateMock);
    }

    private static Stream<Arguments> testArguments() {
        return Stream.of(
                Arguments.of(new User(nickname, null, "fullname", null),
                        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(new User(nickname, "email", null, null),
                        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(new User(nickname, null, null, "about"),
                        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"));
    }

    @ParameterizedTest
    @MethodSource("testArguments")
    void TestChangeWithUpdate(User user, String expected) {
        UserDAO.Change(user);
        Mockito.verify(jdbcTemplateMock).update(Mockito.eq(expected), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void TestChangeWithoutUpdate() {
        UserDAO.Change(new User(nickname, null, null, null));
        Mockito.verify(jdbcTemplateMock, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}
