package com.hw.db.controllers.DAO;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;


public class ForumDAOTest {

    private ForumDAO forum;

    private JdbcTemplate jdbcMock;

    @BeforeEach
    void init() {
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        forum = new ForumDAO(jdbcMock);
    }

    private static Stream<Arguments> testArguments() {
        String slugTest = "slugTest";
        return Stream.of(
                Arguments.of(slugTest, 44, null, null,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                Arguments.of(slugTest, null, null, null,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Arguments.of(slugTest, 23, "since", true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of(slugTest, null, "since", null,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Arguments.of(slugTest, 24, null, true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of(slugTest, 16, "since", true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of(slugTest, null, "since", true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Arguments.of(slugTest, null, null, true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"));
    }

    @ParameterizedTest
    @MethodSource("testArguments")
    void TestUserList(String slug, Integer limit, String since, Boolean desc, String expected) {
        ForumDAO.UserList(slug, limit, since, desc);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(expected),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }
}
