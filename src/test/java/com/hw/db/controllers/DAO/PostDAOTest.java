package com.hw.db.controllers.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.Stream;


public class PostDAOTest {

    private JdbcTemplate jdbcMock;

    private PostDAO postDAO;

    private static String author1 = "author1";

    private static String message1 = "message1";

    private static Timestamp timestamp1 = new Timestamp(1);

    private static String author2 = "author2";

    private static String message2 = "message2";

    private static Timestamp timestamp2 = new Timestamp(2);


    @BeforeEach
    void init() {
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        postDAO = new PostDAO(jdbcMock);
        String sqlQuery = "SELECT * FROM \"posts\" WHERE id=? LIMIT 1;";
        Mockito.when(
                        jdbcMock.queryForObject(
                                Mockito.eq(sqlQuery),
                                Mockito.any(PostDAO.PostMapper.class),
                                Mockito.eq(0)))
                .thenReturn(createPost(author1, message1, timestamp1));
    }

    private static Stream<Arguments> testArgumentsUpdate() {
        return Stream.of(
                Arguments.of(0, createPost(author1, message2, timestamp2),
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(0, createPost(author2, message1, timestamp1),
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Arguments.of(0, createPost(author2, message1, timestamp2),
                        "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(0, createPost(author1, message2, timestamp1),
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(0, createPost(author2, message2, timestamp1),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(0, createPost(author1, message1, timestamp2),
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(0, createPost(author2, message2, timestamp2),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"));
    }

    @ParameterizedTest
    @MethodSource("testArgumentsUpdate")
    void TestSetPostWithUpdate(Integer id, Post post, String expected) {
        PostDAO.setPost(id, post);
        Mockito.verify(jdbcMock).update(Mockito.eq(expected), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void TestSetPostWithoutUpdate() {
        PostDAO.setPost(0, createPost(author1, message1, timestamp1));
        Mockito.verify(jdbcMock, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }

    private static Post createPost(String author, String message, Timestamp ts) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(ts);
        return post;
    }
}
